window.CNN = window.CNN || {};

(function(funcName, baseObj) {
    // The public function name defaults to window.docReady
    // but you can pass in your own object and own function name and those will be used
    // if you want to put them in a different namespace
    funcName = funcName || "docReady";
    baseObj = baseObj || window;
    var readyList = [];
    var readyFired = false;
    var readyEventHandlersInstalled = false;
    
    // call this when the document is ready
    // this function protects itself against being called more than once
    function ready() {
        if (!readyFired) {
            // this must be set to true before we start calling callbacks
            readyFired = true;
            for (var i = 0; i < readyList.length; i++) {
                // if a callback here happens to add new ready handlers,
                // the docReady() function will see that it already fired
                // and will schedule the callback to run right after
                // this event loop finishes so all handlers will still execute
                // in order and no new ones will be added to the readyList
                // while we are processing the list
                readyList[i].fn.call(window, readyList[i].ctx);
            }
            // allow any closures held by these functions to free
            readyList = [];
        }
    }
    
    function readyStateChange() {
        if ( document.readyState === "complete" ) {
            ready();
        }
    }
    
    // This is the one public interface
    // docReady(fn, context);
    // the context argument is optional - if present, it will be passed
    // as an argument to the callback
    baseObj[funcName] = function(callback, context) {
        // if ready has already fired, then just schedule the callback
        // to fire asynchronously, but right away
        if (readyFired) {
            setTimeout(function() {callback(context);}, 1);
            return;
        } else {
            // add the function and context to the list
            readyList.push({fn: callback, ctx: context});
        }
        // if document already ready to go, schedule the ready function to run
        if (document.readyState === "complete") {
            setTimeout(ready, 1);
        } else if (!readyEventHandlersInstalled) {
            // otherwise if we don't have event handlers installed, install them
            if (document.addEventListener) {
                // first choice is DOMContentLoaded event
                document.addEventListener("DOMContentLoaded", ready, false);
                // backup is window load event
                window.addEventListener("load", ready, false);
            } else {
                // must be IE
                document.attachEvent("onreadystatechange", readyStateChange);
                window.attachEvent("onload", ready);
            }
            readyEventHandlersInstalled = true;
        }
    }
})("docReady", CNN);

(function setupMultiAds(ns, win, doc) {
    /**
     * TODO
     * [x] 1. Pull all the data-ad-positions from the DOM and cache it for later.
     * [x] 2. Create a placeholder object that points to indexes into the cached DOM elements.
     * [x] 3. Each data-ad-id has a property in the placholder object as an object with two properties: active and inactive 
     * [x] 4. Create an array of each of the data-ad-id.
     * [x] 5. As the browser is resized cycle through the data-ad-id array and cross examine it with the adGroups object to check that each active ad is still active.
     * [x] 6. If an ad is found to not be active or is missing an active completely then cycle through the inactive for any active ads.
     * [x] 7. If no active ads are found then do nothing.
     * [x] 8. If an active ad is found then set a working flag and create a placeholder ad version for the previously active ad and detach the ad from the DOM. Update the data-ad-position class to the new postion and replace the current placeholder ad with the actual ad. The placeholder's property with inactive and active is updated. At the end clear the working flag.
     * [x] 9. If the working flag is set then no other inspections are checked in the placeholder object.
     * [x] 10. If resizing has stopped right after clearing the working flag. Run the function manually once to ensure nothing was missed.
     */

    ns.MultiAds = ns.MultiAds || {
        /**
         * Holds the NodeList set of DOM references generated by the querySelectorAll.
         */
        domAds: [],
        /**
         * An object where each property is an data-ad-id on the page. Each property 
         * is an object with the properties of active and inactive. Active is the index
         * of the domAd placeholder div that has a content css value of active. Inactive
         * is an array of index values of the domAd placeholder divs that has a content
         * css value of inactive.
         */
        adGroups: {},
        /**
         * Holds the property keys of the adGroups object.
         */
        adGroupIds: [],
        /**
         * The timer used when the page is resized. This will space out the number of times
         * the resizeAds is called while the user resizes the browser.
         */
        resizeTimer: null,
        /**
         * The delay time in milliseconds before the resizeTimer will fire the next resizeAd
         * function call.
         */
        resizeSleep: 100,
        /**
         * This is used to ensure that resizeAd is only ran one at a time. It will allow 
         * for multiple calls to resizeAd but will only run its contents if resizeLock is
         * false.
         */
        resizeLock: false,
        lockMisses: 0,
        bindEvent: function createCrossBrowserEventListener(elem, evt, cb) {
            /* see if the addEventListener function exists on the element */
            if ( elem.addEventListener ) {
                elem.addEventListener(evt,cb,false);
            } else {
                /* if addEventListener is not present, see if this is an IE browser */
                if (elem.attachEvent) {
                    /* prefix the event type with "on" */
                    elem.attachEvent('on' + evt, function simulateAddEventListener() {
                        /**
                         * Use call to simulate addEventListener
                         * This will make sure the callback gets the element for "this"
                         * and will ensure the function's first argument is the event object
                         */
                        cb.call(event.srcElement,event);
		    });
	        }
            }
        },
        sleepAndRun: function ControlsWhenResizeAdsRuns() {
            var _this = this;

            if (_this.resizeTimer) {
                clearTimeout(_this.resizeTimer);
            }

            _this.lockMisses = 0;
            _this.resizeTimer = setTimeout(function fixesContext() {
                if (_this.resizeLock) {
                    _this.lockMisses += 1;
                } else {
                    _this.resizeAds();
                }
            }, _this.resizeSleep);
        },
        resizeAds: function ChecksAdGroupsStateAgainstDOM() {
            var _this = this,
                noActiveAds = -1,
                i,
                ad,
                id,
                inactives,
                j,
                oldActive,
                currentInactive,
                adId,
                activeNotFound,
                hasActiveAd,
                currentAd;

            _this.resizeLock = true;
            /* Cycle through all the groups to ensure the data structure reflects the active DOM */
            for (i = _this.adGroupIds.length - 1; i >= 0; i -= 1) {
                adId = _this.adGroupIds[i];
                id = _this.adGroups[adId].active;
                ad = _this.domAds[id];
                hasActiveAd = (id !== noActiveAds);
                activeNotFound = (!hasActiveAd || (hasActiveAd  && win.getComputedStyle(ad).content !== 'active'));

                if (activeNotFound) {
                    /* The current active placeholer is no longer active. Need to find the new active placeholder. */
                    inactives = _this.adGroups[adId].inactive;
                    oldActive = _this.adGroups[adId].active;
                    j = inactives.length - 1;
                    activeNotFound = true;

                    while (j >= 0 && activeNotFound) {
                        /* see if any of the inactives need to be promoted to an active state. */
                        currentInactive = inactives[j];
                        if (win.getComputedStyle(_this.domAds[currentInactive]).content === 'active') {
                            _this.adGroups[adId].active = currentInactive;

                            if (oldActive !== noActiveAds) {
                                _this.adGroups[adId].inactive.splice(j, 1, oldActive);
                            } else {
                                _this.adGroups[adId].inactive.splice(j, 1);
                            }
                            activeNotFound = false;
                            /* detach the DOM ad and reattach in the correct place */
                            currentAd = doc.getElementById(adId);
                            currentAd.parentNode.removeChild(currentAd);
                            _this.domAds[currentInactive].appendChild(currentAd);
                            currentAd = null;
                        }
                        j -= 1;
                    }

                    if (activeNotFound && (oldActive !== noActiveAds)) {
                        /* An active ad could not be found (a bit werid) so no ad tag moving just updating the adGroups metadata. */
                        _this.adGroups[adId].active = noActiveAds;
                        _this.adGroups[adId].inactive.push(oldActive);
                    }
                }
            }
            _this.resizeLock = false;

            if (_this.lockMisses > 0) {
                _this.resizeAds();
            }
        },
        init: function CheckForMultiAdsOnPage() {
            var _this = this,
                i,
                j,
                ad,
                id,
                activeAdId,
                inactiveAdIds,
                currentGroup,
                currentGroupId,
                inactiveAdCounter,
                currentAd;

            _this.domAds = doc.querySelectorAll('[data-ad-id]');

            /* Create initial adGroups object */
            for (i = _this.domAds.length - 1; i >= 0; i -= 1) {
                ad = _this.domAds[i];
                id = ad.getAttribute('data-ad-id');
                if (!_this.adGroups.hasOwnProperty(id)) {
                    _this.adGroupIds.push(id);
                    _this.adGroups[id] = { active: -1, inactive: [] };
                }

                if (win.getComputedStyle(ad).content === 'active') {
                    _this.adGroups[id].active = i;
                } else {
                    _this.adGroups[id].inactive.push(i);
                }
            }

            /* Move the ad tag to the active placeholder. */
            for (j = _this.adGroupIds.length - 1; j >= 0; j -= 1) {
                currentGroupId = _this.adGroupIds[j];
                currentGroup = _this.adGroups[currentGroupId];
                activeAdId = currentGroup.active;

                if (activeAdId !== -1) {
                    currentAd = doc.getElementById(currentGroupId);
                    if (currentAd.parentNode !== _this.domAds[activeAdId]) {
                        currentAd.parentNode.removeChild(currentAd);
                        _this.domAds[activeAdId].appendChild(currentAd);
                    }
                    currentAd = null;
                }
            }
        }
    }

    ns.docReady(function () { 
        ns.MultiAds.init();
        ns.MultiAds.bindEvent(win, 'resize', function notifyWhenResizeStops() {
            ns.MultiAds.sleepAndRun();
        });
    });
}(CNN, window, document));
