var DEMO = DEMO || {};
(function(funcName, baseObj) {
    "use strict";
    // The public function name defaults to window.docReady
    // but you can modify the last line of this function to pass in a different object or method name
    // if you want to put them in a different namespace and those will be used instead of
    // window.docReady(...)
    funcName = funcName || "docReady";
    baseObj = baseObj || window;
    var readyList = [];
    var readyFired = false;
    var readyEventHandlersInstalled = false;

    // call this when the document is ready
    // this function protects itself against being called more than once
    function ready() {
        if (!readyFired) {
            // this must be set to true before we start calling callbacks
            readyFired = true;
            for (var i = 0; i < readyList.length; i++) {
                // if a callback here happens to add new ready handlers,
                // the docReady() function will see that it already fired
                // and will schedule the callback to run right after
                // this event loop finishes so all handlers will still execute
                // in order and no new ones will be added to the readyList
                // while we are processing the list
                readyList[i].fn.call(window, readyList[i].ctx);
            }
            // allow any closures held by these functions to free
            readyList = [];
        }
    }

    function readyStateChange() {
        if ( document.readyState === "complete" ) {
            ready();
        }
    }

    // This is the one public interface
    // docReady(fn, context);
    // the context argument is optional - if present, it will be passed
    // as an argument to the callback
    baseObj[funcName] = function(callback, context) {
        // if ready has already fired, then just schedule the callback
        // to fire asynchronously, but right away
        if (readyFired) {
            setTimeout(function() {callback(context);}, 1);
            return;
        } else {
            // add the function and context to the list
            readyList.push({fn: callback, ctx: context});
        }
        // if document already ready to go, schedule the ready function to run
        // IE only safe when readyState is "complete", others safe when readyState is "interactive"
        if (document.readyState === "complete" || (!document.attachEvent && document.readyState === "interactive")) {
            setTimeout(ready, 1);
        } else if (!readyEventHandlersInstalled) {
            // otherwise if we don't have event handlers installed, install them
            if (document.addEventListener) {
                // first choice is DOMContentLoaded event
                document.addEventListener("DOMContentLoaded", ready, false);
                // backup is window load event
                window.addEventListener("load", ready, false);
            } else {
                // must be IE
                document.attachEvent("onreadystatechange", readyStateChange);
                window.attachEvent("onload", ready);
            }
            readyEventHandlersInstalled = true;
        }
    }
}("docReady", DEMO));

/**
 * @module ElementResize
 * @author Daniel Buchner <danieljb2@gmail.com>
 * @description
 * Manages elements that throw an event when the element is resized.
 */
(function createElementResizeListener(ns, win, doc) {
    ns.ElementResize = (function createElementResize() {
        var attachEvent,
            isIE,
            requestFrame,
            cancelFrame;

        /**
         * @member {function|undefined} attachEvent
         *
         * @private
         *
         * @description
         * Saves off the function for attachEvent which is used by older
         * IEs to add event listeners to an element.
         */
        attachEvent = doc.attachEvent;

        /**
         * @member {boolean|number} isIE
         *
         * @private
         *
         * @description
         * A simple test for IE using conditional comments. If
         * conditional comments are allowed then isIE is true otherwise
         * it is 0 (truthiness false).
         */
        isIE = /*@cc_on!@*/0; /* <-- Adds a conditional comment to check for IE. Need the comment preserved! */

        /**
         * @method requestFrame
         *
         * @private
         *
         * @description
         * Returns an appropriate function to use when requesting an animation frame.
         *
         * @returns {function}
         * The specific function to use.
         */
        requestFrame = (function createRAF(){
          var raf = win.requestAnimationFrame || win.mozRequestAnimationFrame || win.webkitRequestAnimationFrame ||
              function rafTimeoutVersion(fn) { return win.setTimeout(fn, 20); };

          return function returnRequestFrame(fn){
              return raf(fn);
          };
        }());

        /**
         * @method cancelFrame
         *
         * @private
         *
         * @description
         * Returns an appropriate function to be able to cancel the current frame opperation.
         *
         * @returns {function}
         * The specific function to use.
         */
        cancelFrame = (function createCancelFrame(){
            var cancel = win.cancelAnimationFrame || win.mozCancelAnimationFrame || win.webkitCancelAnimationFrame ||
                         win.clearTimeout;

            return function returnCancelFrame(id) {
                return cancel(id);
            };
        }());

        /**
         * @method resizeListener
         *
         * @private
         *
         * @description
         * Called by the resize event for each object. Uses
         * requestAnimationFrame (or equivalent function) to throttle
         * the broadcasts back to the element listeners. This way a new
         * broadcast to all the elements wont be made until the last one
         * has finished broadcasting.
         *
         * @param {object} e
         * The event that comes from the resize operation.
         */
        function resizeListener(e) {
            var w = e.target || e.srcElement;

            if (w.__resizeRAF__) {
                cancelFrame(w.__resizeRAF__);
            }

            w.__resizeRAF__ = requestFrame(function callEachListener(){
                var trigger = w.__resizeTrigger__;

                trigger.__resizeListeners__.forEach(function callThisListener(fn){
                    fn.call(trigger, e);
                });
            });
        }

        /**
         * @method objectLoad
         *
         * @private
         *
         * @description
         * A function that runs onload of an object tag that has been
         * added to the page. This is where the raw resize events come
         * from for the element. The this object refers to the object
         * tag element that was added to the element that needs to know
         * when it is being resized.
         *
         * @param {object} e
         * The event object associated with the onload event for the
         * object tag.
         */
        function objectLoad(e) {
            this.contentDocument.defaultView.__resizeTrigger__ = this.__resizeElement__;
            this.contentDocument.defaultView.addEventListener('resize', resizeListener);
        }

        /**
         * @method addResizeListener
         *
         * @public
         *
         * @description
         * Adds the element to the element resize listener list
         * associating the callback function with the element. The
         * callback function fn will be called when it is detected that
         * the element has been resized. The resize done by adding a
         * an object tag as a child of the element and setting it as
         * absolute so it will fill up the parent's area.
         *
         * @param {object} element
         * The element that will be listened to for resize events.
         *
         * @param {function} fn
         * The call back function to execute when a resize event has
         * been detected.
         */
        function addResizeListener(element, fn) {
            var obj;

            if (!element.__resizeListeners__) {
                element.__resizeListeners__ = [];

                if (attachEvent) {
                    element.__resizeTrigger__ = element;
                    element.attachEvent('onresize', resizeListener);
                } else {
                    fastdom.read(function getCSSPosition() {
                        var position = getComputedStyle(element).position;

                        fastdom.write(function addObjectToPage() {
                            fastdom.defer(function setupObject() {
                                if (position == 'static') {
                                    element.style.position = 'relative';
                                }

                                obj = element.__resizeTrigger__ = doc.createElement('object');
                                obj.__resizeElement__ = element;
                                obj.onload = objectLoad;
                                obj.type = 'text/html';
                            });

                            fastdom.defer(function appendObject() {
                                if (isIE) {
                                    element.appendChild(obj);
                                }

                                obj.data = 'about:blank';

                                if (!isIE) {
                                    element.appendChild(obj);
                                }
                            });
                        });
                    });
                }
            }
            element.__resizeListeners__.push(fn);
        }

        /**
         * @method removeResizeListener
         *
         * @public
         *
         * @description
         * Removes the event listener from the element.
         *
         * @param {object} element
         * The element to not listen to anymore.
         *
         * @param {function} fn
         * The function to remove from the event listener.
         */
        function removeResizeListener(element, fn) {
            element.__resizeListeners__.splice(element.__resizeListeners__.indexOf(fn), 1);

            if (!element.__resizeListeners__.length) {
                if (attachEvent) {
                    element.detachEvent('onresize', resizeListener);
                } else {
                    element.__resizeTrigger__.contentDocument.defaultView.removeEventListener('resize', resizeListener);
                    element.__resizeTrigger__ = !element.removeChild(element.__resizeTrigger__);
                }
            }
        }

        return {
            removeResizeListener: removeResizeListener,
            addResizeListener: addResizeListener
        }
    }());
}(DEMO, window, document));

(function createResponsiveModules(ns, doc, win, $j) {
    'use strict';

    ns.ResponsiveModules = (function createAPI() {
        var knownModules = {
                'delegate-tracker': '\/responsive-modules\/modules\/delegate-tracker\/js\/module.js',
                'delegate-tracker-ajax': '\/responsive-modules\/modules\/delegate-tracker-ajax\/js\/module.js',
                'delegate-tracker-ajax-1': '\/responsive-modules\/modules\/delegate-tracker-ajax-1\/js\/module.js',
                'delegate-tracker-ajax-2': '\/responsive-modules\/modules\/delegate-tracker-ajax-2\/js\/module.js',
                'delegate-tracker-ajax-3': '\/responsive-modules\/modules\/delegate-tracker-ajax-3\/js\/module.js',
                'delegate-tracker-ajax-4': '\/responsive-modules\/modules\/delegate-tracker-ajax-4\/js\/module.js',
                'delegate-tracker-ajax-5': '\/responsive-modules\/modules\/delegate-tracker-ajax-5\/js\/module.js',
                'delegate-tracker-ajax-6': '\/responsive-modules\/modules\/delegate-tracker-ajax-6\/js\/module.js',
                'delegate-tracker-ajax-7': '\/responsive-modules\/modules\/delegate-tracker-ajax-7\/js\/module.js',
                'delegate-tracker-ajax-8': '\/responsive-modules\/modules\/delegate-tracker-ajax-8\/js\/module.js',
                'delegate-tracker-ajax-9': '\/responsive-modules\/modules\/delegate-tracker-ajax-9\/js\/module.js',
                'delegate-tracker-ajax-10': '\/responsive-modules\/modules\/delegate-tracker-ajax-10\/js\/module.js',
                'delegate-tracker-hybrid-10': '\/responsive-modules\/modules\/delegate-tracker-hybrid-10\/js\/module.js',
                'delegate-tracker-hybrid-9': '\/responsive-modules\/modules\/delegate-tracker-hybrid-9\/js\/module.js',
                'delegate-tracker-hybrid-8': '\/responsive-modules\/modules\/delegate-tracker-hybrid-8\/js\/module.js',
                'delegate-tracker-hybrid-7': '\/responsive-modules\/modules\/delegate-tracker-hybrid-7\/js\/module.js',
                'delegate-tracker-hybrid-6': '\/responsive-modules\/modules\/delegate-tracker-hybrid-6\/js\/module.js',
                'delegate-tracker-hybrid-5': '\/responsive-modules\/modules\/delegate-tracker-hybrid-5\/js\/module.js',
                'delegate-tracker-hybrid-4': '\/responsive-modules\/modules\/delegate-tracker-hybrid-4\/js\/module.js',
                'delegate-tracker-hybrid-3': '\/responsive-modules\/modules\/delegate-tracker-hybrid-3\/js\/module.js',
                'delegate-tracker-hybrid-2': '\/responsive-modules\/modules\/delegate-tracker-hybrid-2\/js\/module.js',
                'delegate-tracker-hybrid-1': '\/responsive-modules\/modules\/delegate-tracker-hybrid-1\/js\/module.js',
                'delegate-tracker-hybrid': '\/responsive-modules\/modules\/delegate-tracker-hybrid\/js\/module.js'
            };

        function loadScript(scriptURL) {
            fastdom.read(function getHeadElement() {
                var s = doc.createElement('script'),
                    head = doc.getElementsByTagName('head')[0];

                fastdom.write(function addScript() {
                    head.appendChild(s);
                    s.src = scriptURL;
                });
            });
        }

        function makeAjaxModule(placeholder, moduleEntry) {
            loadScript(moduleEntry);
        }

        function makeHybridModule(placeholder, moduleEntry) {
            loadScript(moduleEntry);
        }

        function addModuleToPage(placeholder, moduleName, transportType) {
            window.performance.mark('addModuleToPage:start');
            if (knownModules.hasOwnProperty(moduleName)) {
                switch (transportType) {
                case 'iframe':
                    makeIframeModule(placeholder, knownModules[moduleName]);
                    break;
                case 'object':
                    makeObjectModule(placeholder, knownModules[moduleName]);
                    break;
                case 'ajax':
                    makeAjaxModule(placeholder, knownModules[moduleName]);
                    break;
                case 'hybrid':
                    makeHybridModule(placeholder, knownModules[moduleName]);
                    break;
                }
            }
            window.performance.mark('addModuleToPage:stop');
            window.performance.measure('addModuleToPage-' + transportType + '-' + Math.floor(Math.random() * 10000), 'addModuleToPage:start', 'addModuleToPage:stop');
            window.performance.clearMarks('addModuleToPage:start');
            window.performance.clearMarks('addModuleToPage:stop');
        }

        function batchCreate(modules) {
            fastdom.defer(function doThisBatch() {
                window.performance.mark('doThisBatch:start');
                var i,
                    j,
                    attrs,
                    name,
                    transport;

                for (i = modules.length; i >= 0; i -= 1) {
                    if (modules[i] && typeof modules[i].hasAttributes === 'function' && modules[i].hasAttributes()) {
                        attrs = modules[i].attributes;
                        for (j = attrs.length - 1; j >= 0; j -= 1) {
                            if (attrs[j].name === 'data-responsive-module-transport') {
                                transport = attrs[j].value;
                            }

                            if (attrs[j].name === 'data-responsive-module-name') {
                                name = attrs[j].value;
                            }
                        }

                        addModuleToPage(modules[i], name, transport);
                    }
                }
                window.performance.mark('doThisBatch:stop');
                window.performance.measure('doThisBatch-' + Math.floor(Math.random() * 10000), 'doThisBatch:start', 'doThisBatch:stop');
                window.performance.clearMarks('doThisBatch:start');
                window.performance.clearMarks('doThisBatch:stop');
            });
        }

        function addHTMLToModule(placeholder, html) {
            var added = $j.Deferred();

            fastdom.write(function addHTMLToPage() {
                placeholder.innerHTML = html;
                added.resolve(this);
            }, placeholder);

            return added.promise();
        }

        function addResizerToModule(resized, placeholder) {
            var setResizer = $j.Deferred();

            fastdom.defer(function waitAFrame() {
                ns.ElementResize.addResizeListener(this, resized);
                setResizer.resolve(this);
            }, placeholder);

            return setResizer.promise();
        }

        function addEQ(trackers) {
            var setEQ = $j.Deferred();

            fastdom.defer(function queryTheElements() {
                setEQ.resolve(eqjs.query(trackers));
            });

            return setEQ.promise();
        }

        function create() {
            window.performance.mark('create:start');
            var modules = doc.querySelectorAll('.responsive-module'),
                batchSize = 1;

            modules = [].slice.call(modules);
            while (modules && modules.length > 0) {
                batchCreate(modules.splice(0, batchSize));
            }

            window.performance.mark('create:stop');
            window.performance.measure('create', 'create:start', 'create:stop');
        }

        return {
            create: create,
            loadScript: loadScript,
            addHTMLToModule: addHTMLToModule,
            addResizerToModule: addResizerToModule,
            addEQ: addEQ
        };
    }());

    ns.docReady(ns.ResponsiveModules.create);
}(DEMO, document, window, jQuery));
