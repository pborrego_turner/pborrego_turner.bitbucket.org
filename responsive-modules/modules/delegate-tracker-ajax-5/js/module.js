var DEMO = DEMO || {};

(function kickOffTheModule(ns, doc, $j, fastdom, eqjs) {
    var dimensionTrackers;

    dimensionTrackers = [];

    function resized(trackerId, evnt) {
        var dimensionTracker = dimensionTrackers[trackerId];

        if (dimensionTracker.state() !== 'pending') {
            getDimensionTracker(dimensionTracker, evnt.target, evnt.detail).then(notifyLog);
        }
    }

    function getDimensionTracker(dimensionTracker, element, sizeLabel) {
        dimensionTracker = $j.Deferred();

        fastdom.read(function getTheWidth() {
            var w;

            if (sizeLabel === 'very-small') {
                w = this.clientWidth;
                dimensionTracker.resolve(sizeLabel, w, this.clientHeight);
            } else {
                dimensionTracker.reject(sizeLabel);
            }
        }, element);

        return dimensionTracker.promise();
    }

    function notifyLog(sizeLabel, width, height) {
        console.log('This is a ' + sizeLabel + 'module with a width of ' + width + ' and a height of ' + height);
    }

    function getModuleBreakpoints() {
        var getBreakpointContainers = $j.Deferred();

        fastdom.read(function getModuleStates() {
            getBreakpointContainers.resolve(doc.querySelectorAll('.js-results--delegate-tracker-5[data-eq-pts]'));
        });

        return getBreakpointContainers.promise(); 
    }

    function addResizeListener(placeholder) {
        dimensionTrackers.push($j.Deferred().resolve());
        placeholder.addEventListener('eqResize', $j.proxy(resized, $j, dimensionTrackers.length - 1), false);

        return placeholder;
    }

    function addTheModules(html) {
        var getPlaceholders = $j.Deferred(),
            allPlaceholders = [],
            i;

        fastdom.read(function queryDOMForPlaceholders() {
            getPlaceholders.resolve(doc.querySelectorAll('[data-responsive-module-name="delegate-tracker-ajax-5"]'));
        });

        return getPlaceholders.then(function processThem(placeholders) {
            for (i = placeholders.length - 1; i >= 0; i -= 1) {
                allPlaceholders.push(
                    ns.ResponsiveModules.addHTMLToModule(placeholders[i], html).then(addResizeListener)
                );
            }

            return $j.when.apply($j, allPlaceholders);
        });
    }

    $j.ajax('/responsive-modules/modules/delegate-tracker-ajax-5/module.html')
        .then(addTheModules)
        .then(getModuleBreakpoints)
        .then(ns.ResponsiveModules.addEQ);
}(DEMO, document, jQuery, fastdom, eqjs));
