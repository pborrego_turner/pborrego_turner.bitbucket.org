var DEMO = DEMO || {};

(function kickOffTheModule(ns, doc, $j, fastdom, eqjs) {
    function resized(evnt) {
        /* Code for resize stuff goes here... */
    }

    function addEQ() {
        var setEQ = $j.Deferred();

        fastdom.defer(function waitToAddEQ() {
            fastdom.read(function getModuleStates() {
                var trackers = doc.querySelectorAll('.js-results--delegate-tracker-6[data-eq-pts]');

                fastdom.write(function queryTheElements() {
                    setEQ.resolve(eqjs.query(trackers));
                });
            });
        });

        return setEQ.promise();
    }

    function addTheModules(html) {
        var placeholders = doc.querySelectorAll('[data-responsive-module-name="delegate-tracker-hybrid-6"]'),
            allPlaceholders = [],
            i;
        
        for (i = placeholders.length - 1; i >= 0; i -= 1) {
            allPlaceholders.push(
                ns.ResponsiveModules.addHTMLToModule(placeholders[i], html)
                    .then($j.proxy(ns.ResponsiveModules.addResizerToModule, $j, resized))
            );
        }

        return $j.when.apply($j, allPlaceholders);
    }

    $j.ajax('/responsive-modules/modules/delegate-tracker-hybrid-6/module.html')
        .then(addTheModules)
        .then(addEQ);
}(DEMO, document, jQuery, fastdom, eqjs));
