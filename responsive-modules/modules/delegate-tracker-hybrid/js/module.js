var DEMO = DEMO || {};

(function kickOffTheModule(ns, doc, $j, fastdom, eqjs) {
    var dimensionTracker = $j.Deferred().resolve();

    function resized(evnt) {
        /* Code for resize stuff goes here... */
        if (dimensionTracker.state() !== 'pending') {
            getDimensionTracker(this).then(notifyLog);
        }
    }

    function getDimensionTracker(element) {
        dimensionTracker = $j.Deferred();
        fastdom.read(function getTheWidth() {
            var w = this.clientWidth;

            if (w <= 240) {
                dimensionTracker.resolve(w, this.clientHeight);
            } else {
                dimensionTracker.reject(w, this.clientHeight);
            }
        }, element);

        return dimensionTracker.promise();
    }

    function notifyLog(width, height) {
        console.log('This is a very small module with a width of ' + width + ' and a height of ' + height);
    }

    function addEQ() {
        var setEQ = $j.Deferred();

        fastdom.defer(function waitToAddEQ() {
            fastdom.read(function getModuleStates() {
                var trackers = doc.querySelectorAll('.js-results--delegate-tracker[data-eq-pts]');

                fastdom.write(function queryTheElements() {
                    setEQ.resolve(eqjs.query(trackers));
                });
            });
        });

        return setEQ.promise();
    }

    function addTheModules(html) {
        var placeholders = doc.querySelectorAll('[data-responsive-module-name="delegate-tracker-hybrid"]'),
            allPlaceholders = [],
            i;
        
        for (i = placeholders.length - 1; i >= 0; i -= 1) {
            allPlaceholders.push(
                ns.ResponsiveModules.addHTMLToModule(placeholders[i], html)
                    .then($j.proxy(ns.ResponsiveModules.addResizerToModule, $j, resized))
            );
        }

        return $j.when.apply($j, allPlaceholders);
    }

    $j.ajax('/responsive-modules/modules/delegate-tracker-hybrid/module.html')
        .then(addTheModules)
        .then(addEQ);
}(DEMO, document, jQuery, fastdom, eqjs));
