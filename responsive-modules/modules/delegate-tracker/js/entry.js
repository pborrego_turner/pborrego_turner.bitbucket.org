var DEMO = DEMO || {};

(function communicateWithParent(ns, win, doc, par) {
    ns.DelegateTracker = (function createClass() {
        function height() {
            var module = doc.getElementById('module');

            return module ? module.offsetHeight : 0; 
        }

        function resize() {
            par.postMessage(height(), location.origin);
        }

        return {
            height: height,
            resize: resize
        }
    }());

    win.addEventListener('resize', ns.DelegateTracker.resize);
}(DEMO, window, document, top));
