define(
    [
        './../../js/vendor/jquery/jquery.min.js',
        './../../js/vendor/eq.js/eq.min.js',
        './../../js/vendor/fastdom/index.js',
        './module.handlebars',
        './css/module.scss'
    ],
    function makeDelegateTrackers($j, eqjs, fastdom, template) {
        var doc = document,
            trackers = '.js-politics-module[data-type="delegate-tracker"]';

        function addEQ() {
            eqjs.query(trackers);
        }
 
        function addModule(placeholder, content) {
            var moduleAdded = $j.Deferred();

            fastdom.write(function updateThePlaceholder() {
                placeholder.innerHTML = template(content);
                moduleAdded.resolve(placeholder);
            });

            return moduleAdded.promise();
        }

        function addToPage(content) {
            var placeholders = doc.querySelectorAll(trackers),
                modules = [],
                i;

            for (i = placeholders.length - 1; i >= 0; i -= 1) {
                modules.push(addModule(placeholders[i], content));
            }

            return $j.when.apply($j, modules);
        }

        $j.ajax('../tests/Candidates-R.json').then(addToPage).then(addEQ);
    }
);
