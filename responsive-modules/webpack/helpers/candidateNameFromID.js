var _ = require('../../node_modules/lodash/index.js');

module.exports = function candidateNameFromID(id, options) {
     var candidate = _.filter(options.data._parent.root.candidates, function (d) {
        return d.id === id;
    })[0];

    return '<span class="candidate__name--first">' + candidate.fname + '&nbsp;</span><span class="candidate__name--last">' + candidate.lname + '</span>';
};
