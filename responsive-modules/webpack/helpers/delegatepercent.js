module.exports = function delegatepercent(td, options) {
    return Math.round(100 * +td / options.data._parent.root.td_k) + '%';
};
