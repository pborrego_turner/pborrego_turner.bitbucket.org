Handlebars.registerHelper('date', function (datestring) {
    return moment(dataProcessor.parseDate(datestring)).format('MMM. D');
});

Handlebars.registerHelper('datefromTimestamp', function (timestamp) {
    return moment(timestamp).format('MMM. D');
});

Handlebars.registerHelper('datefromTimestampFriendly', function (datestring) {
    return moment(dataProcessor.parseDate(datestring)).format('MMM. Do');
});

Handlebars.registerHelper('dayOfWeekFromTimestamp', function (datestring) {
    return moment(dataProcessor.parseDate(datestring)).format('dddd');
});

Handlebars.registerHelper('delegatepercent', function (td, options) {
    return Math.round(100 * +td / options.data._parent.root.td_k) + '%';
});
