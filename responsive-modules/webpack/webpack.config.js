module.exports = {
    entry: './delegate-tracker/start-here.js',
    output: {
        path: './delegate-tracker/dist',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.handlebars$/,
                loader: 'handlebars-loader',
                query: {
                    debug: 'debug',
                    helperDirs: [__dirname + '/helpers'],
                    knownHelpers: ['candidateNameFromID.js', 'delegatestowinpercent.js']
                }
            },
            {
                test: /\.scss$/,
                loader: 'style!css!sass?includePaths[]=' + encodeURIComponent(__dirname + '/../bower_components/styleguide/styleguide/src')
            }
        ]
    }
};
